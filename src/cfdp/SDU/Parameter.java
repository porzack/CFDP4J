package cfdp.SDU;

import java.util.ArrayList;

import cfdp.filedirective.ConditionCode;
import cfdp.pdu.innerTypes.FilestoreRequest;
import cfdp.pdu.innerTypes.FilestoreResponse;

/**
 * As of Java 8, enums cannot have generics. As such, this horrible method was decided to be best.
 * Benefits:
 * 		- A standard set of parameters where each data type is correlated to each parameter
 * 		- All possible parameters are easily listable with relevant name
 * 		- All parameters are instantiated in one centralized location
 * 		- Easy to add more parameters for custom implementations or standard updates
 * Cons:
 * 		- It is horribly ugly and requires copy commands 
 * 		- It uses a statically defined array in order to allow for iteration over all parameters
 * 
 * All parameters are outlined in section 3.5 of the CCSDS CFDP blue book. 
 * 
 * If a more elegant solution can be devised, it would be appreciated. 
 * 
 * @author zporter 
 *
 * @param <E>
 */
@SuppressWarnings("rawtypes")
public class Parameter<E> {
	public E data;
	public String ID;
	private Parameter(String name, E in)  {
		this.data=in;
		this.ID=name;
	}
	public Parameter createInstance() {
		Parameter p = new Parameter(this.ID, this.data);
		return p;
	}
	public String getID() {
		return ID;
	}
	public E getData() {
		return data;
	}
	public void setData(E in) {
		this.data=in;
	}
	public boolean isInstanceOf(Parameter other) {
		return other.getID().equals(this.getID());
	}
	public static Parameter<Integer> CFDP_ENTITY_ID = new
			Parameter<Integer>("parameter.CFDP.entity.id",0);
	public static Parameter<String> SRC_FILE_NAME = new 
			Parameter<String>("parameter.source.file.name","");
	public static Parameter<String> DST_FILE_NAME = new 
			Parameter<String>("parameter.destination.file.name","");
	public static Parameter<Integer> SEGMENTATION_CONTROL = 
			new Parameter<Integer>("parameter.segmentation.control",0);
	public static Parameter<ArrayList<Integer>> FAULT_HANDLER_OVERRIDES = 
			new Parameter<ArrayList<Integer>>("parameter.fault.handler.overrides",null);
	public static Parameter<Integer> FLOW_LABEL = 
			new Parameter<Integer>("parameter.flow.label",0);
	public static Parameter<Integer> TRANSMISSION_MODE = 
			new Parameter<Integer>("parameter.transmission.mode",0);
	public static Parameter<ArrayList<String>> MESSAGES_TO_USER = 
			new Parameter<ArrayList<String>>("parameter.messages.to.user",new ArrayList<String>());
	public static Parameter<ArrayList<FilestoreRequest>> FILESTORE_REQUESTS = 
			new Parameter<ArrayList<FilestoreRequest>>("parameter.filestore.requests",new ArrayList<FilestoreRequest>());
	public static Parameter<Integer> TRANSACTION_ID = 
			new Parameter<Integer>("parameter.transaction.id",0);
	public static Parameter<ConditionCode> CONDITION_CODE = 
			new Parameter<ConditionCode>("parameter.condition.code",ConditionCode.NO_ERROR);
	public static Parameter<Integer> FILE_STATUS = 
			new Parameter<Integer>("parameter.file.status",0);
	public static Parameter<Integer> DELIVERY_CODE = 
			new Parameter<Integer>("parameter.delivery.code",0);
	public static Parameter<Integer> STATUS_REPORT = 
			new Parameter<Integer>("parameter.status.report",0);
	public static Parameter<ArrayList<FilestoreResponse>> FILESTORE_RESPONSES = 
			new Parameter<ArrayList<FilestoreResponse>>("parameter.filestore.responses",new ArrayList<FilestoreResponse>());
	public static Parameter<Integer> OFFSET = 
			new Parameter<Integer>("parameter.offset", 0);
	public static Parameter<Integer> LENGTH = 
			new Parameter<Integer>("parameter.length", 0);
	public static Parameter<Integer> PROGRESS = 
			new Parameter<Integer>("parameter.length", 0);
	
	public static final Parameter[] PARAMETER_LIST = {
			CFDP_ENTITY_ID,
			SRC_FILE_NAME,
			DST_FILE_NAME,
			SEGMENTATION_CONTROL,
			FAULT_HANDLER_OVERRIDES,
			FLOW_LABEL,
			TRANSMISSION_MODE,
			MESSAGES_TO_USER,
			FILESTORE_REQUESTS,
			TRANSACTION_ID,
			CONDITION_CODE,
			FILE_STATUS,
			DELIVERY_CODE,
			STATUS_REPORT,
			FILESTORE_RESPONSES,
			OFFSET,
			LENGTH,
			PROGRESS};
}
