package cfdp.SDU;

import cfdp.pdu.innerTypes.TLV;
import cfdp.proxy.ProxyMessage;
import cfdp.proxy.ProxyPutRequestMessage;
import cfdp.tools.ArrayTools;

public class Tester {
	public static void log(String s) {
		System.out.println(s);
	}
	public static void main(String[] args) {
		UT_Data request = UT_Request.PUT.createInstance();
		request.description="dfs";
		Parameter s = Parameter.SRC_FILE_NAME.createInstance();
		s.setData("Samuel");
		request.parameters.add(s);
		
		for(Parameter p : request.parameters) {
			log(p.getID());
			log((String)p.getData());
		}
		log(request.description);
		log (request.getParameter(Parameter.SRC_FILE_NAME).getData()+"");
		log(UT_Request.PUT.description);
		TLV lv = new TLV();
		lv.createFromByteArray(new byte[] {0x1,0x8,'H','e','l','l','o',' ','-','Z'});
		//log(lv.getValueString());
		
		Parameter m = Parameter.SRC_FILE_NAME.createInstance();
		m.setData("Lucas");
		log((String)m.getData());
		log((String)s.getData());
		
		log("-------");
		UT_Data req = UT_Request.PUT.createInstance();
		Parameter a = Parameter.CFDP_ENTITY_ID.createInstance();
		Parameter b = Parameter.SRC_FILE_NAME.createInstance();
		Parameter c = Parameter.DST_FILE_NAME.createInstance();
		Parameter d = Parameter.SEGMENTATION_CONTROL.createInstance();
		a.setData((Integer)987);
		b.setData((String)"The moon");
		c.setData((String)"The earth");
		d.setData((Integer)883);
		req.addParameter(a);
		req.addParameter(b);
		req.addParameter(c);
		req.addParameter(d);
		log("Starting params");
		for(Parameter p : req.parameters) {
			log(p.getID());
			log(p.getData()+"");
		}
		ProxyMessage pm = new ProxyPutRequestMessage();
		byte[] data = pm.transformParametersToMessage(req);
		ArrayTools.printArray(data);
		UT_Data req2 = UT_Request.PUT.createInstance();
		pm.populateParameters(req2, data);
		log("ANTICIPATION:");
		
		for(Parameter p : req2.parameters) {
			log(p.getID());
			log(p.getData()+"");
		}
		req2.removeParam(Parameter.CFDP_ENTITY_ID);
		for(Parameter p : req2.parameters) {
			log(p.getID());
			log(p.getData()+"");
		}
	}
}
