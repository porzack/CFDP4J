package cfdp.SDU;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("rawtypes")
public class UT_Data {
	protected ArrayList<Parameter> parameters = new ArrayList<Parameter>();
	protected String identifier;
	protected String description;
	protected Parameter[] optionalParameters;
	protected Parameter[] requiredParameters;
	protected UT_Data(String identifier, String description, 
			Parameter[] optionalParameters, Parameter[] requiredParameters) {
		this.identifier = identifier;
		this.description=description;
		this.optionalParameters = optionalParameters;
		this.requiredParameters = requiredParameters;

	}
	public Parameter getParameter(Parameter instance) {
		for (Parameter p : parameters) {
			if (p.ID == instance.ID) {
				return p;
			}
		}
		return null;
	}
	public void addParameter(Parameter p) {
		if (this.hasParam(p)) {
			System.out.println("Failed to add same parameter twice:"+p.getID()+" to "+identifier);
			return;
		}
		for(Parameter oparam : optionalParameters) {
			if (p.getID().equals(oparam.getID())) {
				parameters.add(p);
				return;
			}
		}
		for(Parameter oparam : requiredParameters) {
			if (p.getID().equals(oparam.getID())) {
				parameters.add(p);
				return;
			}
		}
		// TODO: Implement standard logger
		System.out.println("Failed to add parameter "+p.getID()+" to request "+identifier+" it is not an "
				+ "optional or required parameter");
	}
	public Parameter[] getRequiredParameters() {
		return requiredParameters;
	}
	public Parameter[] getOptionalParameters() {
		return optionalParameters;
	}
	public String getID() {
		return this.identifier;
	}
	public String getDesc() {
		return this.description;
	}
	public ArrayList<Parameter> getDefinedParameters(){
		return parameters;
	}
	public boolean hasParam(Parameter p) {
		for (Parameter k : parameters) {
			if (k.isInstanceOf(p)) {
				return true;
			}
		}
		return false;
	}
	public void removeParam(Parameter p) {
		int x=0;
		while (x<parameters.size()) {
			Parameter k = parameters.get(x);
			if (k.isInstanceOf(p)) {
				parameters.remove(x);
			}else {
				x++;
			}
		}
		
	}
	/**
	 * I am aware that it does not follow the convention of 'is',
	 * but Parameters is plural. 
	 * @return
	 */
	public boolean areParametersSatisfied() {
		return getUnsatisfiedRequiredParameters().size() == 0 ? true : false;
	}
	
	public ArrayList<Parameter> getUnsatisfiedRequiredParameters(){
		ArrayList<Parameter> params = new ArrayList<Parameter>();
		for (Parameter p : requiredParameters) {
			boolean satisfied = false;
			for (Parameter param : parameters) {
				if (param.isInstanceOf(p)) {
					satisfied=true;
					break;
				}
			}
			if (!satisfied) {
				params.add(p);
			}
		}
		return params;
	}
	public UT_Data createInstance() {
		UT_Data instance = new UT_Data(this.identifier, this.description, 
				this.optionalParameters, this.requiredParameters);
		for (Parameter p : this.parameters) {
			instance.addParameter(p);
		}
		return instance;
	}
}
