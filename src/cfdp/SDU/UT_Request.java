package cfdp.SDU;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * TODO: As it becomes possible, add more optional parameters
 * @see CFDP documentation (3.5.1 through 3.5.5)
 * @author zporter
 *
 */
@SuppressWarnings("rawtypes")
public class UT_Request extends UT_Data{

	protected UT_Request(String identifier, String description, Parameter[] optionalParameters,
			Parameter[] requiredParameters) {
		super(identifier, description, optionalParameters, requiredParameters);
	}
	public static UT_Request PUT = new UT_Request("Put.request", "Description", 
			new Parameter[] {
					Parameter.SRC_FILE_NAME,
					Parameter.DST_FILE_NAME,
					Parameter.SEGMENTATION_CONTROL,
					Parameter.FAULT_HANDLER_OVERRIDES,
					Parameter.FLOW_LABEL,
					Parameter.TRANSMISSION_MODE,
					Parameter.MESSAGES_TO_USER,
					Parameter.FILESTORE_REQUESTS},
			new Parameter[] {
					Parameter.CFDP_ENTITY_ID
			});
	public static UT_Request CANCEL = new UT_Request("Cancel.request", "Description", 
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static UT_Request SUSPEND = new UT_Request("Suspend.request", "Description",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static UT_Request RESUME = new UT_Request("Resume.request", "Description",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static UT_Request REPORT = new UT_Request("Report.request", "Description",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static final UT_Request[] REQUEST_LIST = {
		PUT,
		CANCEL,
		SUSPEND,
		RESUME,
		REPORT,
	};
	
}
