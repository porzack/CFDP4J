package cfdp.SDU;


/**
 * @see CFDP documentation (3.5.6 through 3.5.17)
 * @author zporter
 *
 */
@SuppressWarnings("rawtypes")
public class indication extends UT_Data{
	protected indication(String identifier, String description, Parameter[] optionalParameters,
			Parameter[] requiredParameters) {
		super(identifier, description, optionalParameters, requiredParameters);
	}
	public static indication TRANSACTION = new indication("Transaction.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static indication EOF_SENT = new indication("EOF-Sent.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static indication TRANSACTION_FINISHED = new indication("Transaction-Finished.idication", "",
			new Parameter[] {
					Parameter.FILESTORE_RESPONSES,
					Parameter.STATUS_REPORT
			},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.FILE_STATUS,
					Parameter.DELIVERY_CODE
			});
	public static indication METADATA_RECV = new indication("Metadata-Recv.idication", "",
			new Parameter[] {
					Parameter.SRC_FILE_NAME,
					Parameter.DST_FILE_NAME,
					Parameter.MESSAGES_TO_USER
			},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.CFDP_ENTITY_ID
			});
	public static indication FILE_SEGMENT_RECV = new indication("File-Segment-Recv.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.OFFSET,
					Parameter.LENGTH
			});
	public static indication SUSPENDED = new indication("Suspended.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.CONDITION_CODE
			});
	public static indication RESUMED = new indication("Resumed.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.PROGRESS
			});
	public static indication REPORT = new indication("Report.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.STATUS_REPORT
			});
	public static indication FAULT =  new indication("Fault.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.CONDITION_CODE,
					Parameter.PROGRESS
			});
	public static indication ABANDONED = new indication("Abandoned.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID,
					Parameter.CONDITION_CODE,
					Parameter.PROGRESS
			});
	public static indication TRANSFER_CONSIGNED = new indication("Transfer-Consigned.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static indication EOF_RECV = new indication("EOF-Recv.idication", "",
			new Parameter[] {},
			new Parameter[] {
					Parameter.TRANSACTION_ID
			});
	public static final indication[] INDICATION_LIST = {
		TRANSACTION,
		EOF_SENT,
		TRANSACTION_FINISHED,
		METADATA_RECV,
		FILE_SEGMENT_RECV,
		SUSPENDED,
		RESUMED,
		REPORT,
		FAULT,
		ABANDONED,
		TRANSFER_CONSIGNED,
		EOF_RECV
	};
	
}
