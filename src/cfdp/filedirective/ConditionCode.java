package cfdp.filedirective;

public enum ConditionCode {
	NO_ERROR("conditioncode.no_error",0x00),
	ACK_LIMIT_REACHED("conditioncode.ACK_limit_reached",0x01),
	KEEPALIVE_LIMIT_REACHED("conditioncode.keepalive_limit_reached",0x02),
	INVALID_TRANSMISSION_MODE("conditioncode.invalid_transmission_mode",0x03),
	FILESTORE_REJECTION("conditioncode.filestore_rejection",0x04),
	FILE_CHECKSUM_FAILURE("conditioncode.file_checksum_failure",0x05),
	FILE_SIZE_ERROR("conditioncode.file_size_error",0x06),
	NAK_LIMIT_REACHED("conditioncode.NAK_limit_reached",0x07),
	INACTIVITY_DETECTED("conditioncode.inactivity_detected",0x08),
	INVALID_FILE_STRUCTURE("conditioncode.invalid_file_structure",0x09),
	CHECK_LIMIT_REACHED("conditioncode.check_limit_reached",0x0A),
	// B C and D are reserved
	SUSPEND_REQUEST_RECEIVED("conditioncode.Suspend.request_received",0x0E),
	CANCEL_REQUEST_RECEIVED("conditioncode.Cancle.request_received",0x0F)
	;
	public final String name;
	public final int id;
	private ConditionCode(String name, int val) {
		this.name=name;
		this.id=val;
	}
	public static ConditionCode getCodeWithID(int id) {
		for (ConditionCode c : ConditionCode.values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
