package cfdp.filedirective;

public enum FaultHandlerCode {
	HANDLER_NOTICE_OF_CANCELLATION(0x1),
	HANDLER_NOTICE_OF_SUSPENSION(0x2),
	HANDLER_IGNORE_ERROR(0x3),
	HANDLER_ABANDON_TRANSACTION(0x4),
	;
	public final int id;
	private FaultHandlerCode(int val) {
		this.id=val;
	}
	public static FaultHandlerCode getCodeWithID(int id) {
		for (FaultHandlerCode c : values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
