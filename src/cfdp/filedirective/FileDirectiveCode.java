package cfdp.filedirective;


import cfdp.filedirective.pdu.*;

public enum FileDirectiveCode {
	EOF_PDU("filedirective.EOF_PDU", 0x04, new EOF_PDU()),
	FINISHED_PDU("filedirective.finished_PDU", 0x05, new Finished_PDU()),
	ACK_PDU("filedirective.ACK_PDU", 0x06, new ACK_PDU()),
	METADATA_PDU("filedirective.metadata_PDU", 0x07, new Metadata_PDU()),
	NAK_PDU("filedirective.NAK_PDU", 0x08, new NAK_PDU()),
	PROMPT_PDU("filedirective.prompt_PDU", 0x09, new Prompt_PDU()),
	KEEPALIVE_PDU("filedirective.keepalive_PDU", 0x0C, new KeepAlive_PDU())
	;
	public final String name;
	public final int id;
	private FileDirectiveParameterField instance;
	private FileDirectiveCode(String name, int val, FileDirectiveParameterField instance) {
		this.name=name;
		this.id=val;
		this.instance=instance;
	}
	public FileDirectiveParameterField getNewInstance() {
		try {
			return instance.getClass().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static FileDirectiveCode getCodeWithID(int id) {
		for (FileDirectiveCode c : values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
