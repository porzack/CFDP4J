package cfdp.filedirective;

public enum FilestoreActionCode {
	CREATE_FILE(0x0,false),
	DELETE_FILE(0x1,false),
	RENAME_FILE(0x2,true),
	APPEND_FILE(0x3,true),
	REPLACE_FILE(0x4,false),
	CREATE_DIRECTORY(0x5,false),
	DELETE_DIRECTORY(0x6,false),
	
	// deny = delete if present
	DENY_FILE(0x7,false),
	DENY_DIRECTORY(0x8,false),
	;
	public final int id;
	boolean secondFileNameRequired;
	private FilestoreActionCode(int id, boolean secondFileNameRequired) {
		this.id=id;
		this.secondFileNameRequired=secondFileNameRequired;
	}
	public static FilestoreActionCode getCodeWithID(int id) {
		for (FilestoreActionCode c : values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
