package cfdp.filedirective;
/**
 * Implementation choices:
 * "remove directory" has been changed to "delete directory",
 * 
 * Create and Delete have been changed to include the suffix "_FILE_",
 * That way they allow for create_directory to more logically fit into the mix
 * as defined by deny_file_ and deny_directory_. 
 * 
 * 
 * @see CCSDS 727.0-B-4 (Table 5-18)
 * @author Zack
 *
 */
public enum FilestoreResponseCode {
	CREATE_FILE_SUCCESS("filestoreresponse.create.success",FilestoreActionCode.CREATE_FILE, 0x0),
	CREATE_FILE_NOT_ALLOWED("filestoreresponse.create.not_allowed",FilestoreActionCode.CREATE_FILE, 0x1),
	CREATE_FILE_NOT_PERFORMED("filestoreresponse.create.not_performed",FilestoreActionCode.CREATE_FILE, 0xF),
	
	DELETE_FILE_SUCCESS("filestoreresponse.delete.success",FilestoreActionCode.DELETE_FILE, 0x0),
	DELETE_FILE_NONEXISTENT_FILE("filestoreresponse.delete.nonexistent_file",FilestoreActionCode.DELETE_FILE, 0x1),
	DELETE_FILE_NOT_ALLOWED("filestoreresponse.delete.not_allowed",FilestoreActionCode.DELETE_FILE, 0x2),
	DELETE_FILE_NOT_PERFORMED("filestoreresponse.delete.not_performed",FilestoreActionCode.DELETE_FILE, 0xF),
	
	RENAME_SUCCESS("filestoreresponse.rename.success",FilestoreActionCode.RENAME_FILE, 0x0),
	RENAME_OLD_NONEXISTENT_FILE("filestoreresponse.rename.old_filename_is_nonexistent",FilestoreActionCode.RENAME_FILE, 0x1),
	RENAME_NEW_FILE_ALREADY_EXISTS("filestoreresponse.rename.new_filename_already_exists",FilestoreActionCode.RENAME_FILE, 0x2),
	RENAME_NOT_ALLOWED("filestoreresponse.rename.not_allowed",FilestoreActionCode.RENAME_FILE, 0x3),
	RENAME_NOT_PERFORMED("filestoreresponse.rename.not_performed",FilestoreActionCode.RENAME_FILE, 0xF),
	
	APPEND_SUCCESS("filestoreresponse.append.success",FilestoreActionCode.APPEND_FILE, 0x0),
	APPEND_FILE1_NONEXISTENT("filestoreresponse.append.file1_nonexistent",FilestoreActionCode.APPEND_FILE, 0x1),
	APPEND_FILE2_NONEXISTENT("filestoreresponse.append.file2_nonexistent",FilestoreActionCode.APPEND_FILE, 0x2),
	APPEND_NOT_ALLOWED("filestoreresponse.append.not_allowed",FilestoreActionCode.APPEND_FILE, 0x3),
	APPEND_NOT_PERFORMED("filestoreresponse.append.not_performed",FilestoreActionCode.APPEND_FILE, 0xF),
	
	REPLACE_SUCCESS("filestoreresponse.replace.success",FilestoreActionCode.REPLACE_FILE, 0x0),
	REPLACE_FILE1_NONEXISTENT("filestoreresponse.replace.file1_nonexistent",FilestoreActionCode.REPLACE_FILE, 0x1),
	REPLACE_FILE2_NONEXISTENT("filestoreresponse.replace.file2_nonexistent",FilestoreActionCode.REPLACE_FILE, 0x2),
	REPLACE_NOT_ALLOWED("filestoreresponse.replace.not_allowed",FilestoreActionCode.REPLACE_FILE, 0x3),
	REPLACE_NOT_PERFORMED("filestoreresponse.replace.not_performed",FilestoreActionCode.REPLACE_FILE, 0xF),
	
	CREATE_DIRECTORY_SUCCESS("filestoreresponse.create_directory.success",FilestoreActionCode.CREATE_DIRECTORY, 0x0),
	CREATE_DIRECTORY_CANNOT_BE_CREATED("filestoreresponse.create_directory.cannot_be_created",FilestoreActionCode.CREATE_DIRECTORY, 0x1),
	CREATE_DIRECTORY_NOT_PERFORMED("filestoreresponse.create_directory.not_performed",FilestoreActionCode.CREATE_DIRECTORY, 0xF),
	
	DELETE_DIRECTORY_SUCCESS("filestoreresponse.delete_directory.success",FilestoreActionCode.DELETE_DIRECTORY, 0x0),
	DELETE_DIRECTORY_NONEXISTENT_DIRECTORY("filestoreresponse.delete_directory.nonexistent_directory",FilestoreActionCode.DELETE_DIRECTORY, 0x1),
	DELETE_DIRECTORY_NOT_ALLOWED("filestoreresponse.delete_directory.not_allowed",FilestoreActionCode.DELETE_DIRECTORY, 0x2),
	DELETE_DIRECTORY_NOT_PERFORMED("filestoreresponse.delete_directory.not_performed",FilestoreActionCode.DELETE_DIRECTORY, 0xF),
	
	DENY_FILE_SUCCESS("filestoreresponse.deny_file.success",FilestoreActionCode.DENY_FILE, 0x0),
	DENY_FILE_NOT_ALLOWED("filestoreresponse.deny_file.not_allowed",FilestoreActionCode.DENY_FILE, 0x2),
	DENY_NOT_PERFORMED("filestoreresponse.deny_file.not_performed",FilestoreActionCode.DENY_FILE, 0xF),

	DENY_DIRECTORY_SUCCESS("filestoreresponse.deny_directory.success",FilestoreActionCode.DENY_DIRECTORY, 0x0),
	DENY_DIRECTORY_NOT_ALLOWED("filestoreresponse.deny_directory.not_allowed",FilestoreActionCode.DENY_DIRECTORY, 0x2),
	DENY_DIRECTORY_NOT_PERFORMED("filestoreresponse.deny_directory.not_performed",FilestoreActionCode.DENY_DIRECTORY, 0xF),
	
	
	;
	public final String name;
	public final FilestoreActionCode actionCode;
	public final int statusCodeID;
	private FilestoreResponseCode(String name, FilestoreActionCode code, int statusCode) {
		this.name=name;
		this.actionCode=code;
		this.statusCodeID=statusCode;
	}
	public static FilestoreResponseCode getCodeWithID(FilestoreActionCode actionCode, int statusCode) {
		return getCodeWithID(actionCode.id, statusCode);
	}
	public static FilestoreResponseCode getCodeWithID(int actionID, int statusCode) {
		for (FilestoreResponseCode c : values()) {
			if (c.actionCode.id == actionID && c.statusCodeID == statusCode) {
				return c;
			}
		}
		return null;
	}
}
