package cfdp.filedirective;

public enum FinishedFileStatus {
	STATUS_DISCARDED_DELIBERATELY(0b00),
	STATUS_DISCARDED_DUE_TO_REJECTION(0b01),
	STATUS_RETAINED_SUCCESSFULLY(0b10),
	STATUS_UNREPORTED(0b11)
	;
	public final int id;
	private FinishedFileStatus(int id) {
		this.id=id;
	}
	public static FinishedFileStatus getStatusWithID(int id) {
		for (FinishedFileStatus c : values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
