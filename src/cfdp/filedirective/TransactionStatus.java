package cfdp.filedirective;

public enum TransactionStatus {
	
	STATUS_UNDEFINED("trasaction.status.undefined", 0b00),
	STATUS_ACTIVE("transaction.status.active", 0b01),
	STATUS_TERMINATED("transaction.status.terminated",0b10),
	STATUS_UNRECOGNIZED("transaction.status.unrecognized", 0b11)
	;
	public final String name;
	public final int id;
	private TransactionStatus(String name, int val) {
		this.name=name;
		this.id=val;
	}
	public String getName() {
		return name;
	}
	public static TransactionStatus getCodeWithID(int id) {
		for (TransactionStatus c : TransactionStatus.values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
