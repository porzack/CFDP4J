package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;

import cfdp.filedirective.ConditionCode;
import cfdp.filedirective.FileDirectiveCode;
import cfdp.filedirective.TransactionStatus;

public class ACK_PDU extends FileDirectiveParameterField{
	FileDirectiveCode directiveCode; 
	int directiveSubtypeCode;
	ConditionCode conditionCode;
	TransactionStatus transactionStatus;
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		byte first = data.get();
		byte second = data.get();
		directiveCode = FileDirectiveCode.getCodeWithID((first >>> 4));
		directiveSubtypeCode = (first & 0x0F);
		conditionCode = ConditionCode.getCodeWithID(second >>> 4);
		transactionStatus = TransactionStatus.getCodeWithID(second & 0x03);
		return true;
	}

	@Override
	public ByteBuffer toByteBuffer() {
		byte first = 0;
		byte second = 0;
		first = (byte) (directiveCode.id & 0x0F);
		first<<=4;
		first = (byte) (first | (directiveSubtypeCode & 0x0F));
		second = (byte) (conditionCode.id & 0x0F);
		second<<=4;
		second = (byte) (second | (transactionStatus.id & 0x03));
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.put(first);
		buffer.put(second);
		return buffer;
	}
	
}
