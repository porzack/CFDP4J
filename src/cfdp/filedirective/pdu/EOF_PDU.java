package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;

import cfdp.filedirective.ConditionCode;
import cfdp.pdu.innerTypes.EntityIDTLV;
import cfdp.pdu.innerTypes.TLV;

public class EOF_PDU extends FileDirectiveParameterField{
	ConditionCode conditioncode;
	int spare;
	int checksum;
	int filesize;
	TLV faultLocationTLV;
	boolean initialized=false;
	
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		byte first = data.get();
		this.conditioncode = ConditionCode.getCodeWithID((first & 0xF0));
		this.spare = first & 0x0F;
		this.checksum = data.getInt();
		this.filesize = data.getInt();
		if (data.hasRemaining()) {
			if (conditioncode.equals(ConditionCode.NO_ERROR)) {
				System.out.println("EOF_PDU this error should not happen. remaining data with NO_ERROR");
			}
			byte[] tlvdata = new byte[data.remaining()];
			data.get(tlvdata, data.position(), data.limit());
			this.faultLocationTLV = new TLV(tlvdata);
		}
		this.initialized=true;
		return true;
	}
	@Override
	public ByteBuffer toByteBuffer() {
		byte first = 0;
		first = (byte) (conditioncode.id & 0x0F);
		first<<=4;
		first = (byte) (first | (spare & 0x0F));
		int length = 9;
		if (faultLocationTLV != null) {
			length += faultLocationTLV.getTLVLength();
		}
		ByteBuffer data = ByteBuffer.allocate(length);
		data.put(first);
		data.putInt(checksum);
		data.putInt(filesize);
		if (faultLocationTLV!=null) {
			data.put(faultLocationTLV.toByteArray());
		}
		return data;
	}
	
}
