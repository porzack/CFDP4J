package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;

public abstract class FileDirectiveParameterField {
	public FileDirectiveParameterField() {
		
	}
	public abstract boolean initializeData(ByteBuffer data) ;
	public abstract ByteBuffer toByteBuffer();
	protected void initBuffer(ByteBuffer b) {
		b.rewind();
		// TODO: validity checks
	}
}
