package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import cfdp.filedirective.ConditionCode;
import cfdp.filedirective.FinishedFileStatus;

import cfdp.pdu.innerTypes.TLV;
import cfdp.pdu.innerTypes.TLVID;
import cfdp.tools.ArrayTools;

public class Finished_PDU extends FileDirectiveParameterField{

	ConditionCode conditionCode;
	boolean dataCreatedByEndSystem;
	boolean dataComplete;
	FinishedFileStatus status;
	ArrayList<TLV> filestoreResponses = new ArrayList<TLV>();
	TLV faultLocation;
	
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		byte first = (byte) (data.get() &0xFF);
		int conditionC = (first & 0xF0)>>4;
		int systemStatus = (first & 0x08)>>3;
		int deliveryCode = (first & 0x04)>>2;
		int statusC = (first & 0x03);
		
		this.conditionCode = ConditionCode.getCodeWithID(conditionC);
		this.dataCreatedByEndSystem = (systemStatus == 1);
		this.dataComplete = (deliveryCode == 0);
		this.status = FinishedFileStatus.getStatusWithID(statusC);
		
		if(!data.hasRemaining()) {
			return false;
		}
		byte[] remaining = ArrayTools.grabFromArray(data.array(), data.position(), data.limit());
		while(remaining.length>=3) {
			TLV tlv = new TLV(remaining);
			if (tlv.getType() == TLVID.TLV_FILESTORE_REQUEST.id) {
				filestoreResponses.add(new TLV(remaining));
			}else if (tlv.getType() == TLVID.TLV_ENTITY_ID.id) {
				faultLocation = new TLV(remaining);
				break;
			}
			remaining = ArrayTools.grabFromArray(remaining, tlv.getTLVLength(), remaining.length);
		}
		return true;
	}

	private int calcLength() {
		int length=0;
		length +=1; // first byte
		for (TLV tlv : filestoreResponses) {
			length += tlv.getTLVLength();
		}
		if (faultLocation !=null) {
			length+=faultLocation.getTLVLength();
		}
		return length;
	}
	@Override
	public ByteBuffer toByteBuffer() {
		ByteBuffer buff = ByteBuffer.allocate(this.calcLength());
		byte first = 0;
		byte conditionC =  (byte) ((conditionCode.id & 0x0F) << 4);
		byte systemStatus = (byte) ((this.dataCreatedByEndSystem?1:0) << 3);
		byte deliveryCode = (byte) ((this.dataComplete?0:1) << 2);
		byte filestatus = (byte) (this.status.id & 0x03);
		first |= conditionC;
		first |= systemStatus;
		first |= deliveryCode;
		first |= filestatus;
		buff.put(first);
		for (TLV tlv : filestoreResponses) {
			buff.put(tlv.toByteArray());
		}
		if (faultLocation!=null) {
			buff.put(faultLocation.toByteArray());
		}
		return buff;
	}

}
