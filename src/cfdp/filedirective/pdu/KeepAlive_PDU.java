package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;

public class KeepAlive_PDU extends FileDirectiveParameterField{

	// This is because there are no unsigned types in Java. 
	// DOCUMENTATION ONLY SUPPORTS UP TO 32 BITS
	long progress=0;
	
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		if (data.remaining()<4) {
			// TODO : logging
			System.out.println("KeepAlive PDU failed. requires 32 bits. got less than that");
		}
		progress = data.getInt() & 0xFFFFFFFF;
		return true;
	}

	@Override
	public ByteBuffer toByteBuffer() {
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.putInt((int) (progress & 0xFFFFFFFF)); //<-- TODO: Needs unit overflow testing
		return buff;
	}

}
