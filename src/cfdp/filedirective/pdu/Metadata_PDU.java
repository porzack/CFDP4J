package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import cfdp.pdu.innerTypes.LV;
import cfdp.pdu.innerTypes.TLV;
import cfdp.tools.ArrayTools;

public class Metadata_PDU extends FileDirectiveParameterField{
	// TRUE - Record boundaries respected
	// FALSE - Record boundaries not respected
	boolean segmentationControl = false;
	int filesize = 0 ;
	LV srcFileName = new LV();
	LV dstFileName = new LV();
	ArrayList<TLV> options = new ArrayList<TLV>();
	
	private int length;
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		byte first = data.get();
		this.segmentationControl = (first & 0x80) == 0;
		this.filesize = data.getInt();
		byte[] remaining = ArrayTools.grabFromArray(data.array(), data.position(), data.limit());
		this.srcFileName.createFromByteArray(remaining);
		remaining = ArrayTools.grabFromArray(remaining, this.srcFileName.getLength(), remaining.length);
		this.dstFileName.createFromByteArray(remaining);
		remaining = ArrayTools.grabFromArray(remaining, this.dstFileName.getLength(), remaining.length);
		while (remaining.length>0) {
			TLV option = new TLV();
			option.createFromByteArray(remaining);
			remaining = ArrayTools.grabFromArray(remaining, option.getTLVLength(), remaining.length);
			this.options.add(option);
		}
		return true;
	}
	private void calcLength() {
		length =1; // segcontrol and future use
		length+=8; // filesize
		length+=srcFileName.getLVLength();
		length+=dstFileName.getLVLength();
		for (TLV option : options) {
			length+= option.getTLVLength();
		}
		
	}

	@Override
	public ByteBuffer toByteBuffer() {
		calcLength();
		ByteBuffer buff = ByteBuffer.allocate(length);
		byte first = 0;
		first = (byte) (segmentationControl? 0:1);
		first<<=7;
		buff.put(first);
		buff.putInt(filesize);
		buff.put(srcFileName.toByteArray());
		buff.put(dstFileName.toByteArray());
		for(TLV option : options) {
			buff.put(option.toByteArray());
		}
		return buff;
	}
	

}
