package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import cfdp.pdu.innerTypes.SegmentRequestForm;
import cfdp.tools.ArrayTools;

public class NAK_PDU extends FileDirectiveParameterField{

	long startOfScope, endOfScope;
	ArrayList<SegmentRequestForm> requests = new ArrayList<SegmentRequestForm>();
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		this.startOfScope= data.getInt() & 0xFFFFFFFF;
		this.endOfScope = data.getInt() & 0xFFFFFFFF;
		byte[] remaining = ArrayTools.grabFromArray(data.array(),data.position(),data.limit());
		while(remaining.length>=64) { // For the first one
			byte[] segRequest = ArrayTools.grabFromArray(remaining, 0, 64);
			SegmentRequestForm form = new SegmentRequestForm();
			form.initializeData(ByteBuffer.wrap(segRequest));
			requests.add(form);
			if (remaining.length>=64) {
				remaining = ArrayTools.grabFromArray(remaining, 64, remaining.length);
			}else {
				break;
			}
		}
		return true;
	}

	@Override
	public ByteBuffer toByteBuffer() {
		ByteBuffer buff = ByteBuffer.allocate(8 + requests.size()*8);
		buff.putInt((int)startOfScope);
		buff.putInt((int) endOfScope);
		for (SegmentRequestForm f : requests) {
			buff.putInt((int) f.startOffset);
			buff.putInt((int) f.endOffset);
		}
		return buff;
	}

}
