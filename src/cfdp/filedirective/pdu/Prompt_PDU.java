package cfdp.filedirective.pdu;

import java.nio.ByteBuffer;

public class Prompt_PDU extends FileDirectiveParameterField{
	boolean NAKRequired=false;
	boolean KeepAliveRequired=false;
	@Override
	public boolean initializeData(ByteBuffer data) {
		this.initBuffer(data);
		byte sole = data.get();
		if ((sole & 0x8) == 0) {
			this.NAKRequired=true;
		}else {
			this.KeepAliveRequired=true;
		}
		return false;
	}

	@Override
	public ByteBuffer toByteBuffer() {
		ByteBuffer buff = ByteBuffer.allocate(1);
		byte sole=0;
		if (this.NAKRequired) {
			sole=0;
		}else if (this.KeepAliveRequired) {
			sole=(byte) 0x80;
		}
		buff.put(sole);
		return buff;
	}

}
