package cfdp.pdu;

import java.nio.ByteBuffer;

import cfdp.tools.ArrayTools;

/**
 * TODO: Implement this in the way the PDUs were done. This is insane.
 * 
 * 
 * NOTE: All operations were designed to keep with the standard
 * of using unsigned binary integers. If you notice that an operation
 * may have signed issues, please fix it or contact me. 
 * 
 * 
 * This table is as defined in the CFDP documentation
 * Lengths: (in bits)
 * 
 * BYTE 0:
 * 		Version:3 // version 
 * 		PDUType:1 // 0 = File Directive, 1 = File Data
 * 		Direction:1 // 0 = Toward File Receiver, 1 = Toward File Sender
 * 		TransmissionMode:1  // 0 = Acknowledged, 1 = Unacknowledged
 * 		CRCFlag:1 // 0 = CRC not present, 1 = CRC present
 * 		ReservedForFutureUse:1 
 * 
 * BYTE 1, 2: 
 * 		PDUDataFieldLength:16 // In octets
 * 
 * BYTE 3:
 * 		ReservedForFutureUse:1
 * 		LengthOfEntityIDs:3 // number of octets the entity IDs take up (less one)
 * 		ReservedForFutureUse:1 
 * 		LengthOfTransactionSeqNumber:3 // number of octets the transaction seq takes up (less one)
 * 
 * VARIABLE LENGTH 0:
 * 		Source entity ID: length defined in byte 3
 * VARIABLE LENGTH 1:
 * 		Transaction sequence number: length defined in byte 3
 * VARIABLE LENGTH 2: 
 * 		Destination entity ID: length defined in byte 3
 * 
 * 
 * @see CFDP documentation Table 5-1
 * @see cfdp.types.CFDPType.java
 * 
 * @author zporter 
 *
 */
public class PDUHeader {
	byte[] data = new byte[4];
	byte[] sourceEntityID;
	byte[] destinationEntityID;
	byte[] transactionSeqNumber;
	int totalLength = -1;
	
	public PDUHeader(byte[] data, byte[] srcEntityID, byte[] dstEntityID, byte[] transNumber) {
		this.data=data;
		this.sourceEntityID=srcEntityID;
		this.destinationEntityID=dstEntityID;
		this.transactionSeqNumber=transNumber;
		this.recalcTotalLength();
	}
	public PDUHeader(byte[] message) {
		if (message.length < 4) {
			System.out.println("PDUHeader creation failed. Length less than 4");
		}
		int pos = 0;
		this.data=ArrayTools.grabFromArray(message, pos, pos+4);
		pos+=4;
		int entityLength = this.getEntityIDLength()+1;
		int transLength = this.getTransactionSeqLength()+1;
		this.sourceEntityID = ArrayTools.grabFromArray(message, pos,pos+entityLength);
		pos+=entityLength;
		this.transactionSeqNumber = ArrayTools.grabFromArray(message, pos, pos+transLength);
		pos+=transLength;
		this.destinationEntityID = ArrayTools.grabFromArray(message, pos, pos+entityLength);
		this.recalcTotalLength();
		
	}

	
	public boolean setVersion(int value) {
		if (value > 0x7 || value < 0) {
			// TODO: proper error logging
			System.out.println("set version cannot take anything greater than 7 or less than 0");
			return false;
		}
		data[0] = (byte) (data[0] & ~0xE0);
		value = value << 5;
		data[0] = (byte) (data[0] | value);
		return true;
	}
	public boolean setPDUType(int value) {
		if (value>1 || value<0) {
			System.out.println("PDU type must be either 1 or 0");
			return false;
		}
		data[0] = (byte) (data[0] & ~0x10);
		value = value << 4;
		data[0] = (byte) (data[0] | value);
		return true;
	}
	public boolean setDirection(int value) {
		if (value>1 || value<0) {
			System.out.println("Direction must be either 1 or 0");
			return false;
		}
		data[0] = (byte) (data[0] & ~0x08);
		value = value << 3;
		data[0] = (byte) (data[0] | value);
		return true;
	}
	public boolean setTransmissionMode(int value) {
		if (value>1 || value<0) {
			System.out.println("Transmission mode type must be either 1 or 0");
			return false;
		}
		data[0] = (byte) (data[0] & ~0x04);
		value = value << 2;
		data[0] = (byte) (data[0] | value);
		return true;
	}
	public boolean setCRCFlag(int value) {
		if (value>1 || value<0) {
			System.out.println("CRC flag type must be either 1 or 0");
			return false;
		}
		data[0] = (byte) (data[0] & ~0x02);
		value = value << 1;
		data[0] = (byte) (data[0] | value);
		return true;
	}
	// 1 BIT RESERVED FOR FUTURE USE
	
	public boolean setPDUDataFieldLength(int value) {
		if (value>0xFFFF || value<0) {
			System.out.println("PDU type must be less than 0xFFFF and greater than 0");
			return false;
		}
		data[1] = (byte) (value >>> 8);
		data[2] = (byte) (value & 0xFF);
		return true;
	}
	
	// 1 BIT RESERVED FOR FUTURE USE
	public boolean setEntityIDLength(int value) {
		if (value>0x07 || value<0) {
			System.out.println("Entity ID Length must be between 0x07 or 0");
			return false;
		}
		data[3] = (byte) (data[3] & ~0x70);
		value = value << 4;
		data[3] = (byte) (data[3] | value);
		this.recalcTotalLength();
		return true;
	}
	// 1 BIT RESERVED FOR FUTURE USE
	public boolean setTransactionSeqLength(int value) {
		if (value>0x07 || value<0) {
			System.out.println("Transaction Sequence Length type must be between 0x07 or 0");
			return false;
		}
		data[3] = (byte) (data[3] & 0x07);
		data[3] = (byte) (data[3] | value);
		this.recalcTotalLength();
		return true;
	}
	
	public int getVersion() {
		return ((data[0] & 0xE0) >> 5);
	}
	public int getPDUType() {
		return ((data[0] & 0x10) >> 4);
	}
	public int getDirection() {
		return ((data[0] & 0x08) >> 3);
	}
	public int getTransmissionMode() {
		return ((data[0] & 0x04) >> 2);
	}
	public int getCRCFlag() {
		return ((data[0] & 0x02) >> 1);
	}
	public int getPDUDataFieldLength() {
		int val = 0;
		val = val | data[1];
		val<<=4;
		val = val | data[2];
		return (val);
	}
	public int getEntityIDLength() {
		return ((data[3] & 0x70) >> 4);
	}
	public int getTransactionSeqLength() {
		return ((data[3] & 0x07));
	}
	
	
	public boolean setSourceEntityID(long value) {
		int length = getEntityIDLength()+1;
		if (value > Math.pow(2, 8*length) || value < 0) {
			System.out.println("Invalid source entity id. Its value: "+value+" requires more than "+length+" bytes");
			return false;
		}
		ByteBuffer buffer = ByteBuffer.allocate(length);
		buffer.putLong(value);
		byte[] result = buffer.array();
		if (result.length!=length) {
			System.out.println("Source entity id set failed. Different lengths");
			return false;
		}
		this.sourceEntityID = result;
		return true;
	}
	
	
	public boolean setTransactionSeqNumber(long value) {
		int length = this.getTransactionSeqLength()+1;
		if (value > Math.pow(2, 8*length) || value < 0) {
			System.out.println("Invalid transaction seq number. Its value: "+value+" requires more than "+length+" bytes");
			return false;
		}
		ByteBuffer buffer = ByteBuffer.allocate(length);
		buffer.putLong(value);
		byte[] result = buffer.array();
		if (result.length!=length) {
			System.out.println("Transaction seq set failed. Different lengths");
			return false;
		}
		this.transactionSeqNumber = result;
		return true;
	}
	public boolean setDestinationEntityID(long value) {
		int length = getEntityIDLength()+1;
		if (value > Math.pow(2, 8*length) || value < 0) {
			System.out.println("Invalid Destination entity id. Its value: "+value+" requires more than "+length+" bytes");
			return false;
		}
		ByteBuffer buffer = ByteBuffer.allocate(length);
		buffer.putLong(value);
		byte[] result = buffer.array();
		if (result.length!=length) {
			System.out.println("Destination entity id set failed. Different lengths");
			return false;
		}
		this.destinationEntityID = result;
		return true;
	}
	private void recalcTotalLength() {
		this.totalLength = 4+
				(this.getEntityIDLength()+1)*2+
				(this.getTransactionSeqLength() + 1) ;
	}
	
}
