package cfdp.pdu.innerTypes;

public abstract class CFDPDatatype {
	public CFDPDatatype() {
		
	}
	public CFDPDatatype(byte[] in) {
		this.init(in);
	}
	public abstract void init(byte[] in);
	public abstract byte[] toByteArray();
	
	
}
