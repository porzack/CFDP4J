package cfdp.pdu.innerTypes;

import cfdp.filedirective.ConditionCode;
import cfdp.filedirective.FaultHandlerCode;

public class FaultHandlerOverride extends CFDPDatatype{
	public ConditionCode conditionCode;
	public FaultHandlerCode handlerCode;
	public void init(byte[] value) {
		byte sole = value[0];
		int conditionC = (sole & 0xF0)>>>4;
		int handlerC = sole & 0x0F;
		this.conditionCode = ConditionCode.getCodeWithID(conditionC);
		this.handlerCode = FaultHandlerCode.getCodeWithID(handlerC);
	}
	public byte[] toByteArray() {
		byte sole = 0;
		sole =  (byte) ( this.conditionCode.id & 0x0F);
		sole <<=4;
		sole = (byte) (sole | (this.handlerCode.id & 0x0F));
		byte[] value = new byte[]{sole};
		return value;
		
	}
}
