package cfdp.pdu.innerTypes;

import cfdp.filedirective.FilestoreActionCode;
import cfdp.tools.ArrayTools;

public class FilestoreRequest extends CFDPDatatype{
	public FilestoreActionCode actionCode;
	public LV firstFileName;
	public LV secondFileName;
	@Override
	public void init(byte[] value) {
		byte[] data = ArrayTools.clone(value);
		int ac = (data[0] & 0xF0)>>>4;
		this.actionCode = FilestoreActionCode.getCodeWithID(ac);
		data = ArrayTools.grabFromArray(data, 1, data.length);
		this.firstFileName = new LV(data);
		data = ArrayTools.grabFromArray(data, this.firstFileName.getLVLength(), data.length);
		this.secondFileName = new LV(data);
		
	}
	@Override
	public byte[] toByteArray() {
		byte[] value = new byte[1];
		value[0] = (byte) (this.actionCode.id & 0x0F);
		value[0]<<=4;
		byte[] fname = this.firstFileName.toByteArray();
		byte[] sname = this.secondFileName.toByteArray();
		value = ArrayTools.appendArray(value, fname);
		value = ArrayTools.appendArray(value, sname);
		return null;
	}
}
