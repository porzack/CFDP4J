package cfdp.pdu.innerTypes;

import cfdp.filedirective.FilestoreResponseCode;
import cfdp.tools.ArrayTools;

public class FilestoreResponse extends CFDPDatatype{
	public FilestoreResponseCode responseCode;
	public LV firstFileName;
	public LV secondFileName;
	public LV filestoreMessage;
	@Override
	public void init(byte[] value) {
		byte[] data = ArrayTools.clone(value);
		int actionc = (data[0] & 0xF0)>>>4;
		int statusc = data[0] & 0x0F;
		this.responseCode = FilestoreResponseCode.getCodeWithID(actionc, statusc);
		data = ArrayTools.grabFromArray(data, 1, data.length);
		this.firstFileName = new LV(data);
		data = ArrayTools.grabFromArray(data, this.firstFileName.getLVLength(), data.length);
		this.secondFileName = new LV(data);
		data = ArrayTools.grabFromArray(data, this.secondFileName.getLVLength(), data.length);
		this.filestoreMessage = new LV(data);
		
	}
	@Override
	public byte[] toByteArray() {
		byte[] value = new byte[1];
		value[0] = (byte) (this.responseCode.actionCode.id & 0x0F);
		value[0]<<=4;
		value[0] = (byte) (value[0] | (this.responseCode.statusCodeID & 0x0F));
		byte[] fname = this.firstFileName.toByteArray();
		byte[] sname = this.secondFileName.toByteArray();
		byte[] msg = this.filestoreMessage.toByteArray();
 		value = ArrayTools.appendArray(value, fname);
		value = ArrayTools.appendArray(value, sname);
		value = ArrayTools.appendArray(value, msg);
		return value;
	}
}
