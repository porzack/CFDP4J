package cfdp.pdu.innerTypes;

import java.nio.ByteBuffer;

//LengthValue
public class LV {
	private int length=-1; // 0 - 255
	private byte[] value;
	public LV(int length) {
		this.setLength(length);
	}
	public LV(byte[] in) {
		this.createFromByteArray(in);
	}
	public LV() {
		
	}
	public int getLength() {
		return length;
	}
	public byte[] getValue() {
		return this.value;
	}
	public int getValueInt() {
		ByteBuffer b = ByteBuffer.wrap(value);
		return b.getInt();
	}
	public long getValueLong() {
		ByteBuffer b = ByteBuffer.wrap(value);
		return b.getLong();
	}
	public String getValueString() {
		ByteBuffer b = ByteBuffer.wrap(value);
		return new String(b.array());
	}
	public int getLVLength() {
		return this.toByteArray().length;
	}
	public boolean createFromByteArray(byte[] in) {
		setLength(in[0] & 0xFF);
		value = new byte[length];
		for (int x = 0; x<length; x++) {
			value[x]=in[x+1];
		}
		return true;
	}
	public boolean setLength(int length) {
		if (length>255 || length<0) {
			System.out.println("LV: Invalid length. Greater than 255 or less than 0");
			return false;
		}
		this.length=length;
		return true;
	}
	// each int must be a byte
	public boolean setValue(byte[] data) {
		value=data;
		this.length= data.length;
		return true;
	}
	public boolean setValueInt(int data) {
		byte[] val = ByteBuffer.allocate(4).putInt(data).array();
		return this.setValue(val);
	}
	public boolean setValueLong(long data) {
		byte[] val = ByteBuffer.allocate(8).putLong(data).array();
		return this.setValue(val);
	}
	public boolean setValueString(String data) {
		byte[] str = new byte[data.length()];
		int x=0;
		for (char c : data.toCharArray()) {
			str[x++]=(byte)c;
		}
		return this.setValue(str);
	}
	public byte[] toByteArray() {
		int blength = length+1;
		byte[] b = new byte[blength];
		b[0] = (byte) (length & 0xFF);
		for (int x=0; x<length; x++) {
			b[x+1] = (byte) (value[x] & 0xFF);
		}
		return b;
	}
	private boolean isLengthSet() {
		return !(length == -1);
	}
	
}
