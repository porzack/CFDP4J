package cfdp.pdu.innerTypes;

public class MessageToUser extends CFDPDatatype{
	public int messageType;
	@Override
	public void init(byte[] value) {
		boolean forThisType=true;
		forThisType = value[0] == 'c'?forThisType:false;
		forThisType = value[1] == 'f'?forThisType:false;
		forThisType = value[2] == 'd'?forThisType:false;
		forThisType = value[3] == 'p'?forThisType:false;
		this.messageType=value[4] & 0xFF;
		
	}

	@Override
	public byte[] toByteArray() {
		byte[] value = new byte[5];
		value[0] = 'c';
		value[1] = 'f';
		value[2] = 'd';
		value[3] = 'p';
		value[4] = (byte) (this.messageType & 0xFF);
		return value;
	}

}
