package cfdp.pdu.innerTypes;

import java.nio.ByteBuffer;

public class SegmentRequestForm {
	// EACH IS 32 bits, which means a signed int wont work. 
	public long startOffset, endOffset;
	public boolean initializeData(ByteBuffer data) {
		if (data.remaining()<8) {
			// TODO : Logging
			System.out.println("SegmentRequestForm initialize failed. SIZE:"+data.remaining()+" required: 8 octets!");
		}
		startOffset = data.getInt() & 0xFFFFFFFF;
		endOffset = data.getInt() & 0xFFFFFFFF;
		return true;
	}
	public ByteBuffer toByteBuffer() {
		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.putInt((int)startOffset);
		buff.putInt((int)endOffset);
		return buff;
	}
}
