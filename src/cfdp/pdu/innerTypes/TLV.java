package cfdp.pdu.innerTypes;
//TypeLengthValue
public class TLV {
	int type=-1; // 0 - 255
	int length=-1; // 0 - 255
	byte[] value;
	public TLV(int length) {
		this.setLength(length);
	}
	public TLV(byte[] in) {
		this.createFromByteArray(in);
		
	}
	public TLV() {
		
	}
	public boolean createFromByteArray(byte[] in) {
		if (!setType(in[0] & 0xFF)) return false;
		if (!setLength(in[1] & 0xFF)) return false;
		value = new byte[length];
		for (int x = 0; x<length; x++) {
			value[x]=in[x+2];
		}
		this.initializeCFDPDatatype();
		return true;
	}
	public int getLength() {
		return length;
	}
	public byte[] getValue() {
		return value;
	}
	public int getType() {
		return type;
	}
	public int getTLVLength() {
		return this.toByteArray().length;
	}
	public boolean setLength(int length) {
		if (length>255 || length<0) {
			System.out.println("TLV: Invalid length. Greater than 255 or less than 0");
			return false;
		}
		this.length=length;
		return true;
	}
	public boolean setType(int type) {
		if (type>255 || type<0) {
			System.out.println("TLV: Invalid type. Greater than 255 or less than 0");
			return false;
		}
		this.type=type;
		return true;
	}
	// each int must be a byte
	public boolean setValue(byte[] data) {
		if ((data.length!=this.length) && this.isLengthSet()) {
			return false;
		}
		value=data;
		return true;
	}
	public byte[] toByteArray() {
		this.initializeValue();
		int blength = length+2;
		byte[] b = new byte[blength];
		b[0] = (byte) (type & 0xFF);
		b[1] = (byte) (length & 0xFF);
		for (int x=0; x<length; x++) {
			b[x+2] = (byte) (value[x] & 0xFF);
		}
		return b;
	}
	private boolean isLengthSet() {
		return !(length == -1);
	}
	private boolean isTypeSet() {
		return !(type == -1);
	}
	CFDPDatatype datatype;
	protected boolean initializeCFDPDatatype() {
		TLVID id = TLVID.getIDWithID(this.type);
		datatype = id.createInstance();
		datatype.init(value);
		return true;
	}
	protected void initializeValue() {
		this.value = datatype.toByteArray();
		this.length = value.length;
	}
}

