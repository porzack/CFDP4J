package cfdp.pdu.innerTypes;

import cfdp.filedirective.FileDirectiveCode;

public enum TLVID {
	TLV_FILESTORE_REQUEST(0x00, new FilestoreRequest()),
	TLV_FILESTORE_RESPONSE(0x01, new FilestoreResponse()),
	TLV_MESSAGE_TO_USER(0x02, new MessageToUser()),
	
	TLV_FAULT_HANDLER_OVERRIDE(0x04, new FaultHandlerOverride()),
	TLV_FLOW_LABEL(0x05, new FlowLabel()),
	TLV_ENTITY_ID(0x06, new EntityID()),
	
	TLV_GENERIC(0xF, null)
	;
	public final int id;
	public final CFDPDatatype datatype;
	private TLVID(int id, CFDPDatatype type) {
		this.id=id;
		this.datatype=type;
	}
	public CFDPDatatype createInstance() {
		try {
			return this.datatype.getClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static TLVID getIDWithID(int id) {
		for (TLVID c : values()) {
			if (c.id == id) {
				return c;
			}
		}
		return null;
	}
}
