package cfdp.proxy;

import cfdp.SDU.UT_Data;
import cfdp.SDU.UT_Request;
import cfdp.filedirective.ConditionCode;
import cfdp.filedirective.FaultHandlerCode;
import cfdp.pdu.innerTypes.FaultHandlerOverride;

public class ProxyFaultHandlerOverrideMessage extends ProxyMessage{
	@Override
	public void populateParameters(UT_Data params, byte[] data) {
		FaultHandlerOverride fault = new FaultHandlerOverride();
		fault.init(data);
		
	}

	@Override
	public byte[] transformParametersToMessage(UT_Data params) {
		// TODO Auto-generated method stub
		return null;
	}


}