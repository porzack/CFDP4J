package cfdp.proxy;

import cfdp.SDU.UT_Data;
import cfdp.pdu.innerTypes.FilestoreRequestTLV;
import cfdp.pdu.innerTypes.TLVID;
import cfdp.tools.ArrayTools;

public class ProxyFilestoreRequestMessage extends ProxyMessage{
	@Override
	public void populateParameters(UT_Data params, byte[] data) {
		byte[] type = new byte[] {(byte)(TLVID.TLV_FILESTORE_REQUEST.id & 0xFF)};
		data = ArrayTools.appendArray(type, data);
		FilestoreRequestTLV tlv = new FilestoreRequestTLV(data);
		
		// TODO: DO STUFF WITH ME
		
	}

	@Override
	public byte[] transformParametersToMessage(UT_Data params) {
		// TODO Auto-generated method stub
		return null;
	}


}