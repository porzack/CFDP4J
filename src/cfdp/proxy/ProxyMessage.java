package cfdp.proxy;

import cfdp.SDU.UT_Data;
import cfdp.SDU.UT_Request;

/**
 * These really are quite simple classes and everything done here COULD
 * be done via a single class with static methods. But given the variability 
 * of the data, having to have a id->class map is a pain, but less of a 
 * pain than an id->function map which would have to support both functions
 * and possibly more as the protocol is expanded upon. Yes, having 10 .java
 * files is worse than having 1, but having one massive list of static functions
 * is just bad practice. 
 * 
 * 
 * @author Zack
 *
 */
public abstract class ProxyMessage {
	public abstract void populateParameters(UT_Data params, byte[] data);
	public abstract byte[] transformParametersToMessage(UT_Data params);
}
