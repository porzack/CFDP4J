package cfdp.proxy;

import java.util.ArrayList;

import cfdp.SDU.Parameter;
import cfdp.SDU.UT_Data;
import cfdp.pdu.innerTypes.LV;

public class ProxyMessageToUserMessage extends ProxyMessage{
	@Override
	public void populateParameters(UT_Data params, byte[] data) {
		LV msg = new LV();
		msg.createFromByteArray(data);
		ArrayList<String> msges;
		if (params.hasParam(Parameter.MESSAGES_TO_USER)) {
			msges = (ArrayList<String>)params.getParameter(Parameter.MESSAGES_TO_USER).getData();
		}else {
			msges = new ArrayList<String>();
		}
		msges.add(msg.getValueString());
		Parameter pmsg = Parameter.MESSAGES_TO_USER.createInstance();
		pmsg.setData(msges);
		params.addParameter(pmsg);
		
	}

	@Override
	public byte[] transformParametersToMessage(UT_Data params) {
		LV msg = new LV();
		ArrayList<String> list = (ArrayList<String>)params.getParameter(Parameter.MESSAGES_TO_USER).getData();
		msg.setValueString(list.get(0));
		byte[] data = msg.toByteArray();
		return data;
	}


}