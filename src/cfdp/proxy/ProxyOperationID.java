package cfdp.proxy;

public enum ProxyOperationID {
	PROXY_PUT(0x00),
	PROXY_MESSAGE_TO_USER(0x01),
	PROXY_FILESTORE_REQUEST(0x02),
	PROXY_FAULT_HANDLER_OVERRIDE(0x03),
	PROXY_TRANSMISSION_MODE(0x04),
	PROXY_FLOW_LABEL(0x05),
	PROXY_SEGMENTATION_CONTROL(0x06),
	PROXY_PUT_RESPONSE(0x07),
	PROXY_FILESTORE_RESPONSE(0x08),
	PROXY_PUT_CANCEL(0x09),
	
	ORIGINATING_TRANSACTION_ID(0x0A),
	
	
;
	public final int id;
	private ProxyOperationID(int id){
		this.id=id;
	}
}
