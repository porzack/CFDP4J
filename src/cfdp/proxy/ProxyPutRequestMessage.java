package cfdp.proxy;

import cfdp.SDU.Parameter;
import cfdp.SDU.UT_Data;
import cfdp.SDU.UT_Request;
import cfdp.pdu.innerTypes.LV;
import cfdp.tools.ArrayTools;

public class ProxyPutRequestMessage extends ProxyMessage{

	@Override
	public void populateParameters(UT_Data params, byte[] data) {
		LV dst = new LV();
		LV srcFileName = new LV();
		LV dstFileName = new LV();
		byte[] remaining = ArrayTools.clone(data);
		dst.createFromByteArray(remaining);
		remaining = ArrayTools.grabFromArray(remaining, dst.getLVLength(), remaining.length);
		srcFileName.createFromByteArray(remaining);
		remaining = ArrayTools.grabFromArray(remaining, srcFileName.getLVLength(), remaining.length);
		dstFileName.createFromByteArray(remaining);
		
		Parameter pdst = Parameter.CFDP_ENTITY_ID.createInstance();
		Parameter psrcName = Parameter.SRC_FILE_NAME.createInstance();
		Parameter pdstName = Parameter.DST_FILE_NAME.createInstance();
		pdst.setData(dst.getValueInt());
		psrcName.setData(srcFileName.getValueString());
		pdstName.setData(dstFileName.getValueString());
		params.addParameter(pdst);
		params.addParameter(psrcName);
		params.addParameter(pdstName);
		
	}

	@Override
	public byte[] transformParametersToMessage(UT_Data params) {
		LV dst = new LV();
		LV srcFileName = new LV();
		LV dstFileName = new LV();
		dst.setValueInt((Integer)params.getParameter(Parameter.CFDP_ENTITY_ID).getData());
		srcFileName.setValueString((String)params.getParameter(Parameter.SRC_FILE_NAME).getData());
		dstFileName.setValueString((String)params.getParameter(Parameter.DST_FILE_NAME).getData());
		byte[] msg = new byte[0];
		msg = ArrayTools.appendArray(msg, dst.toByteArray());
		msg = ArrayTools.appendArray(msg, srcFileName.toByteArray());
		msg = ArrayTools.appendArray(msg, dstFileName.toByteArray());
		return msg;
	}


}