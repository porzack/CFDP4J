package cfdp.tools;

public class ArrayTools {
	public static byte[] grabFromArray(byte[] message, int start, int end) {
		byte[] result = new byte[end-start];
		for (int x=start; x<end; x++) {
			result[x-start]=message[x];
		}
		return result;
	}
	public static byte[] clone(byte[] in ) {
		byte[] out = new byte[in.length];
		int x=0;
		for (byte b : in) {
			out[x++]=b;
		}
		return out;
	}
	public static byte[] appendArray(byte[] a, byte[] b) {
		byte[] out = new byte[a.length+b.length];
		int x=0;
		for (byte by : a) {
			out[x++]=by;
		}
		for (byte by : b) {
			out[x++]=by;
		}
		return out;
	}
	public static void printArray(byte[] array) {
		System.out.print("{");
		for (byte b : array) {
			System.out.print(byteToHex(b)+", ");
		}
		System.out.println("}");
	}
	
	public static String byteToHex(byte b) {
		return String.format("%02X", b);
	}
 }
