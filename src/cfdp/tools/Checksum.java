package cfdp.tools;

import java.util.ArrayList;

public class Checksum {
	/**
	 * 
	 * 
	 * @see CFDP documentation (4.1.2) 
	 * @param data
	 * @return
	 */
	public static int calculateChecksum(byte[] data) {
		// it shall initially be set to all 'zeroes'
		int value = 0;
		

		// each 4-octet shall be constructed by copying some octet of file data, 
		// whose offset within the file is an integral multiple of 4, into the first (high order)
		// octet of the word, and copying the next three octets of file data into the next three
		// octets of the word
		ArrayList<Integer> words = new ArrayList<Integer>();
		int word = 0;
		for (int pos = 0; pos < data.length; pos++) {
			int wordpos = pos % 4;
			if (wordpos == 0) {
				word = 0; 
			}
			// (java doesnt support unsigned types)
			word = word | ((int)data[pos]);
			word = word << 4;
			if (wordpos == 3) {
				words.add(word);
			}
		}
		
		// it shall be calculated by modulo 2^32 addition of all 4 octet words (int in Java)
		// aligned from the start of the file
		
		// the results of the addition shall be carried into each available octet of the checksum 
		// unless the addition overflows the checksum length, in which case carry shall be disregarded
		
		for (Integer w : words) {
			value = (int) ((w+value) % 4294967296L);
		}
		return value;
	}
}
